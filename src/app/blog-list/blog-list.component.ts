import {Component} from '@angular/core';
import {BlogServicesService} from "../blog-services.service.ts.service";
import {BehaviorSubject} from "rxjs";
import {Blog} from "../blog";
import {Dialog} from "@angular/cdk/dialog";

@Component({
  selector: 'app-blog-list',
  templateUrl: './blog-list.component.html',
  styleUrls: ['./blog-list.component.scss']
})
export class BlogListComponent {

  blog = new BehaviorSubject<Blog[]>([])
  paginator = new BehaviorSubject<any>(null)
  page = 1;
  qtd = 10;


  constructor(private _blog: BlogServicesService, private dialog: Dialog) {

    this.getAll(this.page, this.qtd);


  }


  async getAll(page: number | string, qtd: number | string) {

    await this._blog.getAll(qtd, page).subscribe(v => {

      if (v.data) {
        this.blog.next(v.data)
        this.paginator.next(v.links)

      } else {

        throw new Error('erro')

      }


    }, err => {

      alert('Erro ao criar usuário.')
      // this.dialog.open(ConfirmDialogComponent, {
      //   data: {
      //     msg: err.msg || 'Erro ao criar usuário.',
      //     textCancel: 'voltar',
      //     textOK: 'OK',
      //   }
      // })

    })
  }

}

