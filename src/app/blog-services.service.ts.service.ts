import {Injectable} from '@angular/core';
import {catchError, Observable, throwError} from "rxjs";
import {HttpClient, HttpErrorResponse, HttpHeaders} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class BlogServicesService {

  constructor(private http: HttpClient) {
  }


  getAll(qtd: any, page: any): Observable<any> {

    let url = 'http://localhost/api-laravel/blog';

    let options = {
      headers: new HttpHeaders({
        'Content-type': 'application/json',
      })
    }
    let data = {
      qtd: qtd,
      page: page,
    }

    return this.http.post(url, data, options).pipe(catchError((error: HttpErrorResponse) => {
      return throwError(() => {
        return error.error
      });
    }))


  }


}
