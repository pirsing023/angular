import {Component} from '@angular/core';
import {FormControl, FormGroup, FormGroupDirective, Validators} from "@angular/forms";
import {DadosService} from '../c-table/dados.service';

@Component({
  selector: 'app-c-filho',
  templateUrl: './c-filho.component.html',
  styleUrls: ['./c-filho.component.scss'],
})
export class CFilhoComponent {

  formGroup = new FormGroup({
    nome: new FormControl({value: '', disabled: false}, [
      Validators.required,
      Validators.minLength(10)
    ]),
    email: new FormControl({value: '', disabled: false}, [
      Validators.required,
      Validators.email
    ]),
    tel: new FormControl({value: '', disabled: false}),
    endereco: new FormControl({value: '', disabled: false})
  });

  constructor(private dadosService: DadosService) {
  }

  submit(f: FormGroupDirective) {
    this.cadastrar();
  }

  cadastrar() {
    const nome = this.formGroup.get('nome')?.value;
    const email = this.formGroup.get('email')?.value;

    if (this.formGroup.invalid) {
      return
    }

    const newData = {
      nome,
      email,
      tel: this.formGroup.get('tel')?.value || '',
      endereco: this.formGroup.get('endereco')?.value || '',
    };

    const existingDataString = localStorage.getItem('formData');
    let existingData: any[] = existingDataString ? JSON.parse(existingDataString) : [];

    existingData.push(newData);

    localStorage.setItem('formData', JSON.stringify(existingData));

    // Atualiza os dados no servi�o para notificar a grid
    this.dadosService.atualizarDados(existingData);

    this.formGroup.reset();

  }

}
