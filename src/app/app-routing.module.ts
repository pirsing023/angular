import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CFilhoComponent } from './c-filho/c-filho.component';
import { CTable } from './c-table/c-table.component';
import { LoginComponent } from './login/login.component';
import { BlogListComponent } from './blog-list/blog-list.component';

const routes: Routes = [
  { path: 'c-filho', component: CFilhoComponent },
  { path: 'c-table', component: CTable },
  { path: 'blog', component: BlogListComponent },
  { path: 'LoginComponent', component: LoginComponent},
  { path: '', redirectTo: '/LoginComponent', pathMatch: 'full' }, // Redirecionamento padr�o
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
