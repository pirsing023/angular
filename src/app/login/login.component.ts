import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../auth.service'; // Importe o serviço AuthService
import { Router } from '@angular/router';
import {HttpClient, HttpErrorResponse, HttpHeaders} from "@angular/common/http";
import {MatDialog} from "@angular/material/dialog";
import {DialogData} from "../confirm/confirm.component";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent {
  constructor(
    private AuthService: AuthService,
    private router: Router
  ) {
  }

  pageCadastro: boolean = false

  loginFG = new FormGroup({
    email: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required]),
  });

  cadastroFG = new FormGroup({
    name: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required]),
  });

  changePage() {
    this.pageCadastro = !this.pageCadastro;
  }


  async login() {

    let data = {
      email: this.loginFG.controls.email.value,
      password: this.loginFG.controls.password.value,
    }


    await this.AuthService.login(data).subscribe(v => {
      if (!v.error) {
        alert('Usuário Logado com sucesso!');

        window.localStorage.setItem('login', 'true');
        this.router.navigate(['/blog'])

      } else {
        alert('erro')
        // this.dialog.open(ConfirmDialogComponent, {
        //   data: {
        //     msg: v.msg || 'Erro ao efetuar login.',
        //     textCancel: 'voltar',
        //     textOK: 'OK',
        //   }
        // })
      }

    })


  }

  async cadastro() {

    let data = {
      name: this.cadastroFG.controls.name.value,
      email: this.cadastroFG.controls.email.value,
      password: this.cadastroFG.controls.password.value,
    }


    await this.AuthService.cadastro(data).subscribe(v => {

      console.log(v);
      if (!v.error) {

        alert('Usuário cadastrado com sucesso!');

        // let dialog = this.dialog.open(ConfirmDialogComponent, {
        //   data: {msg: 'Usuário cadastrado com sucesso!', textCancel: 'voltar', textOK: 'OK',}
        // })


        // dialog.afterClosed().subscribe(v => {
        //   this.cadastroFG.reset();
        //   this.changePage()
        // })


      } else {

        throw new Error('erro')

      }


    }, err => {
      console.log(err);
      alert('Erro ao criar usuário.')
      // this.dialog.open(ConfirmDialogComponent, {
      //   data: {
      //     msg: err.msg || 'Erro ao criar usuário.',
      //     textCancel: 'voltar',
      //     textOK: 'OK',
      //   }
      // })
    })


  }


}
