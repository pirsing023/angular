import { TestBed } from '@angular/core/testing';

import { BlogServicesServiceTsService } from './blog-services.service.ts.service';

describe('BlogServicesServiceTsService', () => {
  let service: BlogServicesServiceTsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BlogServicesServiceTsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
