export interface Blog {
    id: number;
    title: string;
    description: string;
    text: string;
    image: string;
    updated_at: string;
    created_at: string;
  
}
