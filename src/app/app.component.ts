import {Component, ViewChild} from '@angular/core';
import {FormControl, FormGroup, FormGroupDirective, NgForm, Validators} from "@angular/forms";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})


export class AppComponent {


  constructor() {

    this.obterLista()
  }

  title = 'meu-projeto';
  size: string = '16px';
  class: boolean = true
  ngIfText: boolean = true;

  list: any[] = [
    {nome: 'Danilo Fernando', idade: '30'},
    {nome: 'Murillo Lima', idade: '35'},
    {nome: 'Gabreil K.', idade: '23'},
    {nome: 'Marcos Túlio', idade: '22'},
  ]

  disabledInput: boolean = true;
  switch: string = 'type1';


  nomeCompleto: string = '';
  campoNgForNome: string = '';
  campoNgForIdade: string = '';

  formGroup = new FormGroup({
    subject: new FormControl({value: '', disabled: false}, [
      Validators.required,
      Validators.minLength(3),
    ]),
    note: new FormControl('Anotação aqui')
  })
  Notes: any[] = [];


  fontSize(size: string) {
    this.size = size;
  }

  changeClass() {

    this.class = !this.class;
  }

  changeNgIf() {
    this.ngIfText = !this.ngIfText
  }


  changeDisabled() {
    this.disabledInput = !this.disabledInput
  }

  changeSwicth(type: string) {

    this.switch = type

  }

  inserirList(nome: any, idade: any) {

    this.list.push({
      nome: nome,
      idade: idade.toString()
    })

    window.localStorage.setItem('lista', JSON.stringify(this.list))

    this.campoNgForNome = ''
    this.campoNgForIdade = ''


  }

  private obterLista() {
    let list = window.localStorage.getItem('lista')

    if (list) {
      this.list = JSON.parse(list);
    }
  }

  removeList(index: any) {

    this.list.splice(index, 1)
    window.localStorage.setItem('lista', JSON.stringify(this.list))
  }


  submit(f: FormGroupDirective) {

    this.addNote()

    f.resetForm()

  }

  addNote() {

    let value = {
      subject: this.formGroup.get('subject')?.value,
      note: this.formGroup.get('note')?.value,
    }

    this.Notes.push(value)

  }



}
