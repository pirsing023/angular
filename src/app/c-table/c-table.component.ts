import {Component, OnInit} from '@angular/core';
import {DadosService} from './dados.service';
import {MatDialog} from "@angular/material/dialog";
import {ConfirmComponent} from "../confirm/confirm.component";

interface DadosTabela {
  nome: string;
  email: string;
  tel: string;
  endereco: string;
}

@Component({
  selector: 'app-c-table',
  templateUrl: './c-table.component.html',
  styleUrls: ['./c-table.component.scss']
})
export class CTable implements OnInit {
  dados: DadosTabela[] = [];

  constructor(private dadosService: DadosService, public dialog: MatDialog) {
  }

  ngOnInit() {
    // Obt�m os dados do localStorage ao iniciar o projeto
    const existingDataString = localStorage.getItem('formData');
    if (existingDataString) {
      this.dados = JSON.parse(existingDataString);
      // Atualiza os dados no servi�o para notificar a grid
      this.dadosService.atualizarDados(this.dados);
    }

    this.dadosService.dados$.subscribe((novosDados) => {
      this.dados = novosDados;
    });
  }

  excluirDado(index: number) {

    let dialogRemove = this.dialog.open(ConfirmComponent, {
      data: {
        msg: 'Deseja remover o contato?',
        txtCancel: 'Cancelar',
        txtOk: 'Remover',
      }

    })

    dialogRemove.afterClosed().subscribe(value => {
      if (value) {
        this.dadosService.excluirDado(index);
      }
    })

  }
}
