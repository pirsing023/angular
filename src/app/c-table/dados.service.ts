import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class DadosService {
  private dadosSubject = new BehaviorSubject<any[]>([]);
  dados$ = this.dadosSubject.asObservable();

  constructor() {}

  atualizarDados(novosDados: any[]) {
    this.dadosSubject.next(novosDados);
  }

  excluirDado(index: number) {
    let existingDataString = localStorage.getItem('formData');
    let existingData: any[] = existingDataString ? JSON.parse(existingDataString) : [];

    if (index >= 0 && index < existingData.length) {
      existingData.splice(index, 1);
      localStorage.setItem('formData', JSON.stringify(existingData));
      this.atualizarDados(existingData); 
    }
  }
}