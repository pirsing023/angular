import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { C1Component } from './c1.component';

describe('AppComponent', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [RouterTestingModule],
    declarations: [C1Component]
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(C1Component);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'meu-projeto'`, () => {
    const fixture = TestBed.createComponent(C1Component);
    const app = fixture.componentInstance;
    expect(app.title).toEqual('meu-projeto');
  });

  it('should render title', () => {
    const fixture = TestBed.createComponent(C1Component);
    fixture.detectChanges();
    const compiled = fixture.nativeElement as HTMLElement;
    expect(compiled.querySelector('.content span')?.textContent).toContain('meu-projeto app is running!');
  });
});
